package com.company.keo_kay;

public class Main{
    public static  void runThread1(){
        String greeting="Hello KSHRD!";
        try {

            for ( int i=0; i<greeting.length(); i++){

                System.out.print(greeting.charAt(i));
                Thread.sleep(300);
            }
        }catch (InterruptedException e ){

            System.out.println("Thread1 is interrupted ");
        }
    }
   //printing stars
    public static void runThread2(){
        String star= "******************************************";

        System.out.println("");

        try{
            for (int i = 10; i< star.length(); i++){

                System.out.print(star.charAt(i));
                Thread.sleep(300);
            }

        }catch (InterruptedException e){
            System.out.println("runThread2 got interrupted");
        }




    }
    public  static  void runThread3(){
        String commitSt="I will try my best here at HRD";
        try {
            System.out.println();
            for (int i=0; i<commitSt.length(); i++){
                System.out.print(commitSt.charAt(i));
                Thread.sleep(300);
            }

        }catch (InterruptedException e ){


        }
    }

    public static  void runThread4(){
        String minus="-----------------------------------";
        try {
            System.out.println();
            for (int i=0; i<minus.length(); i++){
                System.out.print(minus.charAt(i));
                Thread.sleep(300);
            }

        }catch (InterruptedException e ){
            System.out.println(" Thread4 is interrupted ...");

        }
    }
    public static void runThread5(){
    String download ="Downloading";
        System.out.print("\n"+download);

       try  {
           for (int i=0; i<10; i++){

               System.out.print(".");
               Thread.sleep(300);
           }

       }catch (InterruptedException e){

           System.out.println("Thread 5 is interrupted");
       }

        System.out.println("Completed 100%");

    }

    public static void main(String[] args) {

        // using Method reference
         Runnable runnable1= Main::runThread1;
         Runnable runnable2 = Main::runThread2;
         Runnable runnable3 = Main::runThread3;
         Runnable runnable4 = Main::runThread4;
         Runnable runnable5= Main::runThread5;

        Thread thread1 = new Thread(runnable1);
        Thread thread2 = new Thread(runnable2);
        Thread thread3 = new Thread(runnable3);
        Thread thread4= new Thread(runnable4);
        Thread thread5 = new Thread(runnable5);
        // let call it to work :
       try{
           thread1.start();
           thread1.join();
           thread2.start();
           thread2.join();
           thread3.start();
           thread3.join();
           thread4.start();
           thread4.join();
           thread5.start();
          // thread5.join();

       }catch (InterruptedException e ){
           System.out.println("Our threads got interrupted ");
       }
    }

}
